"""
merge webpage and its associated folder into a single unit to reduce
clutter on hardisk and avoid false positives in duplicate file scans

DO NOT DELETE ANY DATA DESTRUCTIVELY, REARRANGE/REGROUP ONLY

## Vocab for this project

non-htm:
  files that are not HTML, e.g. source code files, mp3s, PDFs, etc.
complete:
  a set of a html source file & its corresponding folder of the fashion: `<filename>.htm[l]` && `<filename>_files/`
incomplete:
  if the html source file is missing from "complete" i.e. `<filename>.htm[l]` doesn't exist but `<filename>_files/` exists
  these occur whenever there's an incomplete download of webpage
  it is ASSUMED that any directory whose name ends with `_files` must be a part of incomplete download
  TODO: implement a feature that makes code inspect innards of said folder to make sure it contains stuff like .js .css .png or empty
txt-only-htm:
  if the corresponding folder is missing from "complete" (i.e. `<filename>_files/` doesn't exist but only `<filename>.htm[l]` exists)
  in rare cases (e.g. some university professor's webpage, my notes, etc.) we find these
subFolder:
  any sub directory that doesn't qualify as "incomplete"

TODO: pause execution for large folder warning
TODO: disk IO is the enemy, but try multithread-izing whenever possible
"""

import os
import sys
import json
import shutil
import hashlib
import logging
import zipfile
import platform
import subprocess
from datetime import datetime
from typing import Dict, List, Set


METADATA_FILENAME = "fs-metadata.json"
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


class Job:
	def __init__(self, title, contents, webpage, metadata):
		self._wp = webpage
		self.title = title
		self.contents = contents
		self.metadata = metadata
		return

	def sha(self):
		hv = hashlib.sha256()
		for a_file in self.contents:
			with open(a_file, mode="rb") as fo:
				hv.update(fo.read())
		return hv.hexdigest()


class WebPage:
	""" a simple struct to hold webpage info """
	s2i = lambda self, s: int(hashlib.sha256(s.encode()).hexdigest(), 16) # put sys.getdefaultencoding() in s.encode?

	def __init__(self, htm_file: str, companion_dir: str):
		self.htm_file = htm_file or ""
		self.companion_dir = companion_dir or ""
		return

	def __hash__(self):
		return self.s2i(self.htm_file) + self.s2i(self.companion_dir)

	def __repr__(self):
		return f"<WebPage htm_file={self.htm_file}, companion_dir={self.companion_dir}>"

	def __eq__(self, o):
		if isinstance(o, type(self)):
			return hash(self) == hash(o)
		else:
			raise RuntimeError("Can't compare Apples to Oranges")




verbose = False

def dump_os_stat(stat: os.stat_result):
	assert isinstance(stat, os.stat_result)
	answer = dict()
	for attr in dir(stat):
		if attr.startswith("st_"):														# https://docs.python.org/3/library/os.html#os.stat
			k = attr
			v = getattr(stat, k)
			answer[k] = v
	return answer

def yyyymmddHMS(timestamp):
    sort_friendly_fmt = r"%Y/%m/%d@%H:%M:%S"
    return datetime.fromtimestamp(timestamp).strftime(sort_friendly_fmt)

def win_set_cdate(filename, timestamp):
	# from SU/q/292630
	SKELETON = (
		'C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe'
		# '%SYSTEMROOT%\\System32\\WindowsPowerShell\\v1.0\\powershell.exe' giving `ValueError: unsupported format character 'S' (0x53) at index 1`
		' -NoLogo -NoProfile -NonInteractive -Command "& {%s}"'
	)
	PS_DATE_FMT = r"%m/%d/%Y %I:%M %p"
	timestamp = datetime.fromtimestamp(timestamp).strftime(PS_DATE_FMT)
	ps_cmd1 = f'$(Get-Item \"{filename}\").creationtime=$(Get-Date \"{timestamp}\")' 	# \" is necessary
	ps_cmd2 = f'$(Get-Item \"{filename}\").lastwritetime=$(Get-Date \"{timestamp}\")' 	# \" is necessary
	subprocess.run(SKELETON % ps_cmd1, check=True, capture_output=True)
	subprocess.run(SKELETON % ps_cmd2, check=True, capture_output=True)
	return

def nix_set_cdate(filename, timestamp):
	""" no know method of changing cdate, that's why stat dump in important """
	NIX_DATE_FMT = r"%Y-%m-%d %H:%M:00"													# unix.stackexchange.com/a/250407
	time_string = datetime.fromtimestamp(timestamp).strftime(NIX_DATE_FMT)
	SKELETON = f'touch -m -d "{time_string}" "{filename}"'
	subprocess.run(SKELETON, check=True, capture_output=True, shell=True)
	return

def filename2title(filename: str) -> str:
	if os.sep in filename:
		raise RuntimeError("Need local path not relative or absolute path")
	if os.path.isfile(filename):
		title, extension = os.path.splitext(filename)
		if extension.lower() in [".htm", ".html"]:
			return title
		else:
			return ""
	elif os.path.isdir(filename):
		if filename.endswith("_files"):
			return filename[:-1*len("_files")]                  # see stackoverflow.com/a/32055500,, LESSON: one way & preferably only one correct way to do a thing
		else:
			return ""
	return

def title2filename(title: str, allfiles: List[str]) -> WebPage:
	htm_file = None
	companion_dir = None
	for afile in allfiles:
		if afile.startswith(title):
			if afile.endswith("_files"):
				companion_dir = afile
			elif afile[len(title):].lower() in [".htm", ".html"]:
				htm_file = afile
			else:
				raise RuntimeError(f"No filename starts with '{title}'")
	return WebPage(htm_file, companion_dir)

def some_fn_name(title: str, allfiles: List[str]) -> str:
	for afile in allfiles:
		x, y = os.path.splitext(afile)
		if x == title and y.lower() not in ["htm", "html"]:
			return afile
	return

def generate_tasklist(dir_path: str) -> Dict[str, Set[WebPage]]:
	original_locn = os.getcwd()
	os.chdir(dir_path)

	stuff_here = os.listdir()
	dirs = set(x for x in stuff_here if os.path.isdir(x))         # not an issue cuz duplicate filename are anyway not allowed
	files = set(x for x in stuff_here if os.path.isfile(x))       # not an issue cuz duplicate filename are anyway not allowed
	# assert len(dirs) + len(files) == len(os.listdir())              # just to be sure no surprises like symbolic links are lurking around
	tof_html_files = set(filter(None, map(filename2title, files)))       # assert len(tof_html_files) == len(set(glob.glob("*.htm*")))
	tof_companion_dirs = set(filter(None, map(filename2title, dirs)))    # assert len(dirs) == len(tof_companion_dirs)
	# TODO: @CRITICAL https://gitlab.com/s-c-p/html-compressor/-/jobs/480128974
	""" what if 2 files with same title exists (obviously with different
	extensions), what happens to the set_var.func(other_set) approach? E.g.
	2 files alex.htm (maybe ∈ complete XOR txt_only_htm) and alex.txt exist
	then what... think, TEST
	such title collision cases won't reveal themselves in current test suite
	cuz random_sent_gen() generates & uses rubbish terms
	actually its a very likely case, e.g. I make notes or take a screenshot
	of the webpages & save the .txt/.png with same title
	maybe something like...
	try:
		assert len(files) == len(set(os.path.splitext(x)[0] for x in files))
	except AssertionError:
		print("above described case was encountered, aborting processing, proceed with caution")
	"""
	complete_ones = tof_html_files.intersection(tof_companion_dirs)
	incomplete_ones = tof_companion_dirs.difference(complete_ones)
	txt_only_ones = tof_html_files.difference(complete_ones)
	non_htm_ones = set(
		os.path.splitext(x)[0] for x in files    # TODO: WARNing: see ⑆ 'shdveBeenCore'; all places where splitext is uses as-is (i.e not via filename2title) it is vulnerable to being absolute / relative path
	).difference(tof_html_files)
	partial_fn = lambda title: title2filename(title, stuff_here)
	partial_fn2 = lambda title: some_fn_name(title, files)
	tasklist = {
		"non_htm": set(map(partial_fn2, non_htm_ones)),   # job #483243972@a6e93388 & #483249524@1a11f6f0
		"complete": set(map(partial_fn, complete_ones)),
		"incomplete": set(map(partial_fn, incomplete_ones)),
		"txt_only_htm": set(map(partial_fn, txt_only_ones))
	}

	os.chdir(original_locn)
	return tasklist

def delete(filename):
	# TODO: move to recyclebin?
	if os.path.isfile(filename):
		os.remove(filename)
	elif os.path.isdir(filename):
		shutil.rmtree(filename)
	else:
		pass
	return

def hash_zip(filename):
	hv = hashlib.sha256()
	with zipfile.ZipFile(filename) as zh:
		for i_filename in zh.NameToInfo:
			if i_filename == METADATA_FILENAME:
				continue
			else:
				bin_data = zh.read(zh.NameToInfo[i_filename].filename)
				hv.update(bin_data)
	return hv.hexdigest()

def make_zip(title, contents, metadata):
	zip_name = f"{title}.htmz"
	with zipfile.ZipFile(zip_name, "w") as zh:
		for item in contents:
			zh.write(
				arcname=item, # locn_on_arcv
				filename=item # locn_on_disk
			)
		zh.comment = yyyymmddHMS(metadata["st_ctime"]).encode()
		zh.writestr(METADATA_FILENAME, json.dumps(metadata))
	if platform.system() == "Windows":
		change_CM_date = win_set_cdate
	elif platform.system() == "Darwin":
		NotImplemented
	else:
		change_CM_date = nix_set_cdate
	change_CM_date(zip_name, metadata["st_ctime"])
	return zip_name

def get_contents(path):
	if not path:
		return list()
	if os.sep in path:
		raise RuntimeError("Absolute/Relative paths not allowed")
	contents = list()
	for r, d, f in os.walk(path):
		f = [os.path.join(r, x) for x in f]
		contents += f
	return contents

def generate_actions(tasklist: Set[WebPage]):
	actions = dict()
	actions['ignore'] = tasklist['txt_only_htm'].union(tasklist['non_htm'])
	actions['compress'] = list()
	actionable_items = tasklist['complete'].union(tasklist['incomplete'])
	for item in actionable_items:
		title = filename2title(item.companion_dir)
		contents = get_contents(item.companion_dir)
		fs_metadata = os.stat(item.companion_dir)
		if item.htm_file:
			contents.append(item.htm_file)
		else:
			title = f"(incomplete) {title}"
		actions['compress'].append(
			Job(title, contents, item, fs_metadata)
		)
	return actions

def compress(tasklist):
	actions = generate_actions(tasklist)
	for job in actions['compress']:
		zip_name = make_zip(
			job.title,
			job.contents,
			dump_os_stat(job.metadata)
		)
		if hash_zip(zip_name) == job.sha():
			if verbose:
				print(f"zip integrity check passed, deleting: {job.title}")
			delete(job._wp.htm_file)
			delete(job._wp.companion_dir)
		else:
			logging.critical(f"WARN! Sanitization skipped because integrity check failed for\n:: {job.title}")
	return

def htmz(target_dir):
	target_dir = os.path.abspath(target_dir)
	tasklist = generate_tasklist(target_dir)
	init_dir = os.getcwd()
	os.chdir(target_dir)
	compress(tasklist)
	for i, (_, d, __) in enumerate(os.walk(target_dir)):
		if i > 50:  # avoind going in too deep
			break
		if any(map(lambda x: x.endswith("_files"), d)):
			logging.warning(f"scattered htm files suspected at: {_}")
			break
	os.chdir(init_dir)
	return

def main(dirList: List[str]):
	if platform.system() == "Windows":
		starter = os.startfile
	else:
		starter = lambda x: subprocess.run(f"xdg-open {x}", shell=True)
	for aDir in dirList:
		htmz(aDir)
		try:    starter(aDir)
		except: pass
	return

if __name__ == '__main__':
	main(sys.argv[1:])
