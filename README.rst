Intro
======

A python 3 script to merge webpage and its associated folder into a single unit to reduce clutter on hardisk and avoid false positives in duplicate file scans

Todo
----

- [almost done] maintain all (create, modify[, access]) timestamps
    - *windows testing:* see Appveyor, others at https://github.com/ligurio/awesome-ci
    - powershell is slow as mollases
        - used NirCmd stored as base64 within the code
        - batch powershell commands into robust atomized date-set commands
    - create date ain't  really working (unix.stackexchange.com/q/118577)
    - comments don't show in 7-zip GUI
- publish on PyPI, htmz.py is the only asset, others are for testing
- long path raising WinError3
- abrupt stop (Ctrl+C), do I loose files?
- default to non-subDir scan, subdir scan-&-compress should be declared explicitly by user
