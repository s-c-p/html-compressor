"""
create the followin in a test folder names "lab/"
some subFolder
some non-htm
some complete
some incomplete
some txt-only-htm
"""

import os
import random
from typing import Generator
from english_words import english_words_set

from htmz import WebPage

FLP = "!@#()[]-_+,"    # filename-legal-punctuation https://stackoverflow.com/questions/4814040/allowed-characters-in-filename#4814088
WORDS = list(english_words_set)

def random_term_gen() -> Generator[str, None, None]:
    punc_or_none = lambda: random.choice([
        "",
        "",
        "",
        random.choice(FLP)
    ])
    while True:
        yield punc_or_none() + random.choice(WORDS) + punc_or_none()
    return

def random_sent_gen(min_len: int, max_len: int) -> Generator[str, None, None]:
    term = random_term_gen()
    while True:
        yield " ".join(
            next(term)
            for _ in range(
                random.randint(min_len, max_len)
            )
        )
    return

def make_one_file(title, size=128):
    with open(title, mode="wb") as fo:
        fo.write(os.urandom(size))
    return title

def make_files(count, extensions, size=256):
    made = list()
    sentence = random_sent_gen(1, 9)    # TODO: in htmz, predict name too long & avoid processing those, just report em
    for _ in range(count):
        title = next(sentence) + random.choice(extensions)
        make_one_file(title, size)
        made.append(title)
    return made

def make_companion_dir(title):
    os.mkdir(f"{title}_files")
    os.chdir(f"{title}_files")
    made = make_files(
        random.randint(1, 100),
        [".js", ".css", ".png", ".jpg"],
        128
    )
    os.chdir("..")
    return made

def fill_test_env(complete=11, incomplete=7, txt_only_htm=2, non_htm=5):
    report = dict()
    sentence = random_sent_gen(1, 9)

    report['complete'] = list()
    for _ in range(complete):
        title = next(sentence)
        companion_dir = make_companion_dir(title)
        html_file = make_one_file(title + random.choice([".htm", ".html"]))
        report['complete'].append(
            WebPage(html_file, f"{title}_files")
        )

    report['incomplete'] = list()
    for _ in range(incomplete):
        title = next(sentence)
        companion_dir = make_companion_dir(title)
        report['incomplete'].append(
            WebPage(None, f"{title}_files")
        )

    report['txt_only_htm'] = list()
    for _ in range(txt_only_htm):
        title = next(sentence) + random.choice([".htm", ".html"])
        make_one_file(title)
        report['txt_only_htm'].append(
            WebPage(title, None)
        )

    report['non_htm'] = make_files(non_htm, [".txt", ".pdf", ".mp3"])

    return report

def make_test_env(subFolder=3):
    os.mkdir("lab")
    os.chdir("lab")
    x = fill_test_env()
    report = [[os.getcwd(), x]]
    sentence = random_sent_gen(1, 9)
    for _ in range(subFolder):
        title = next(sentence)
        os.mkdir(title)
        os.chdir(title)
        local_report = fill_test_env()
        os.chdir("..")
        report.append([os.path.abspath(title), local_report])
    os.chdir("..")
    return report
