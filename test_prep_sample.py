import os
import glob
import shutil
import string

import pytest
from english_words import english_words_set

import htmz
import prep_sample as tm

def test_setup():
    assert True
    return

def test_random_term_gen():
    POOL = string.ascii_lowercase \
        + string.ascii_uppercase + tm.FLP + ".'&"           # the 3 symbols found in english_words_set
    term_gen = tm.random_term_gen()
    for _ in range(100):
        term = next(term_gen)
        assert all(x in POOL for x in term)
    return

def test_random_sent_gen():
    g1 = tm.random_sent_gen(4, 4)
    for _ in range(100):
        sent = next(g1)
        assert len(sent.split()) == 4
    g2 = tm.random_sent_gen(1, 10)
    for _ in range(100):
        words = next(g2).split()
        assert len(words) >= 1 and len(words)  <= 10
    return

@pytest.fixture(scope="function")
def temp_test_dir():
    title = "temp_test_dir"
    os.mkdir(title)
    os.chdir(title)
    yield title
    os.chdir("..")
    shutil.rmtree(title)
    return

def test_make_one_file(temp_test_dir):
    tm.make_one_file("hello.jpeg")
    assert os.path.isfile("hello.jpeg")
    assert os.stat("hello.jpeg").st_size == 128
    return

def test_make_files(temp_test_dir):
    size = 64
    count = 3
    extensions = [".xyz", ".abc"]
    reported = tm.make_files(count, extensions, size)
    detected = os.listdir(".")
    assert set(detected) == set(reported)
    assert all(os.stat(f).st_size == size for f in detected)
    return

def test_make_companion_dir(temp_test_dir):
    title = "abcdefg"

    before_dir = os.getcwd()
    reported = tm.make_companion_dir(title)
    after_dir = os.getcwd()
    assert before_dir == after_dir

    made_dir = glob.glob("*_files")
    assert len(made_dir) == 1
    made_dir = made_dir[0]
    assert made_dir == f"{title}_files"
    assert os.path.isdir(made_dir)
    assert set(reported) == set(os.listdir(made_dir))

    files = [os.path.join(made_dir, f) for f in os.listdir(made_dir)]
    assert all(os.stat(f).st_size == 128 for f in files)
    assert len(files) >= 1 and len(files) <= 100

    known_extensions = [".js", ".css", ".png", ".jpg"]
    assert all(os.path.splitext(f)[-1] in known_extensions for f in files)

    return

def test_fill_test_env(temp_test_dir):
    non_htm = 5
    complete = 11
    incomplete = 7
    txt_only_htm = 2
    reported = tm.fill_test_env(complete, incomplete, txt_only_htm, non_htm)
    detected = htmz.generate_tasklist(os.path.abspath("."))
    assert detected["non_htm"] == set(reported['non_htm'])
    assert detected["complete"] == set(reported['complete'])
    assert detected["incomplete"] == set(reported['incomplete'])
    assert detected["txt_only_htm"] == set(reported['txt_only_htm'])
    return

def test_make_test_env(temp_test_dir):
    subFolder = 3
    before_dir = os.getcwd()
    reported = tm.make_test_env(subFolder)
    after_dir = os.getcwd()
    assert before_dir == after_dir
    assert os.path.isdir("lab")
    os.chdir('lab')
    detected = [
        os.path.abspath(f)
        for f in os.listdir()
        if os.path.isdir(f) and \
        not f.endswith("_files")
    ]
    os.chdir("..")
    assert len(detected) == subFolder
    assert set(detected) == set(x[0] for x in reported[1:]) # since first (0th) element is PWD
    return
