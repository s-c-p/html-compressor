import os
import prep_sample
from htmz import generate_tasklist, htmz, WebPage


def touch(fn):
    with open(fn, mode="wt") as fo:
        fo.write("t")
    return

def test_generate_tasklist():
    os.mkdir("lab lab lab")
    os.chdir("lab lab lab")

    os.mkdir("ilex-2")
    touch(   "alex.htm")
    os.mkdir("alex_files")
    touch(   "blex.html")
    os.mkdir("blex_files")
    os.mkdir("clex_files")
    os.mkdir("dlex_files")
    touch(   "elex.htm")
    touch(   "flex.html")
    touch(   "glex.txt")
    touch(   "hlex.pdf")

    os.chdir("..")

    r = generate_tasklist("lab lab lab")
    assert r["non_htm"] == set(["glex.txt", "hlex.pdf"])
    for x in list(r["complete"]) + [WebPage("alex.htm", "alex_files"), WebPage("blex.html", "blex_files")]:
        print(f"{x}: {hash(x)}")
    assert r["complete"] == set([WebPage("alex.htm", "alex_files"), WebPage("blex.html", "blex_files")])
    assert r["incomplete"] == set([WebPage(None, "clex_files"), WebPage(None, "dlex_files")])
    assert r["txt_only_htm"] == set([WebPage("elex.htm", None), WebPage("flex.html", None)])
    return

def test_htmz(caplog):
    report = prep_sample.make_test_env()
    htmz("lab")
    prefix = "scattered htm files suspected at: "
    logged_messages = [tup[-1] for tup in caplog.record_tuples]
    assert len(logged_messages) == 1
    assert logged_messages[0][len(prefix):] in [x[0] for x in report]
    return
