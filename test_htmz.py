import os
import uuid
import shutil

import htmz

def test_real_lifey():
    temp_dir = str(uuid.uuid4())
    shutil.copytree("test-files", temp_dir)

    htmz.htmz(temp_dir)
    os.chdir(temp_dir)

    for r, d, f in os.walk("."):
    	if any([_.endswith("_files") for _ in d]):
    		raise RuntimeError("Test failed")
    	break

    os.chdir("..")

    shutil.rmtree(temp_dir)
    return
